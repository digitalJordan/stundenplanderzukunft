
public abstract class DaoGeneric<A> {
	
	protected final String tablename;	
	protected java.sql.Connection conn;
	
	protected DaoGeneric(java.sql.Connection conn, String tablename) {
		this.tablename = tablename;
		this.conn = conn;
	}
	
	public abstract int count();
	
	public abstract A getRecord(int id);

}
