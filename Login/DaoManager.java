import java.sql.SQLException;

public enum DaoManager {		//DAOManager zust�ndig f�r das �ffnen/Schlie�en der DBVerbindung
	
	INSTANCE;					//darf nur eine Instanz geben, wor�ber man den
								//DaoManager anspricht
	
	//Verbindungsdaten
	private java.sql.Connection conn;
	private  final String url = "jdbc:mysql://";
	private  final String hostname = "127.0.0.1";
	private  final String port = "3307";
	private  final String db_name = "StundenplanderZukunft";
	private  final String username = "root";
	private  final String password = "root123";
	
	//Konstruktor private damit es nur 1 Instanz gibt 
	private DaoManager() {					
		
	}
	
	private String getConnectionUrl() {
		String url = this.url + this.hostname + ":" + this.port +"/" + this.db_name; //Verbindungs-String zusammenbauen
		return url;
	}
	//JDBC-Verbindung
	private java.sql.Connection getConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver");		//Treiber wird geladen
			String connectionUrl = getConnectionUrl();	//auf Verbindungs-String zugreifen
			conn = java.sql.DriverManager.getConnection(connectionUrl, username, password); //DB-Verbindung herstellen
			
			if(conn!=null) {
				System.out.println("Verbindung hergestellt");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Problem bei der Verbindung: " + e.getMessage());
		}
		return conn;
	
	}
	public void open() {		//DB-Verbindung �ffnen
		try {
			if(this.conn==null || this.conn.isClosed()) {
				this.conn = getConnection();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void close() {
		try {
			if(this.conn !=null || this.conn.isClosed()) {
				this.conn.close();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public DaoGeneric getDao(DB_Tabellen table) {
		DaoGeneric dao = null;
		try {
			if(this.conn == null || this.conn.isClosed()) {
				this.open();
			}
			switch(table){
			case USER:
				dao = new DaoUser(this.conn);
				break;
			default:
				dao = null;
				break; 
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}	
		return dao;	
	}

}
