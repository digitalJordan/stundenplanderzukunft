
public class ShutdownHook  extends Thread{
		
	public void run() {
		DaoManager daoMgr = DaoManager.INSTANCE;
		daoMgr.close();
		System.out.println("Verbindung wird getrennt");
	}
}
