
public class Main {

	public static void main(String[] args) {
		
		Runtime.getRuntime().addShutdownHook(new ShutdownHook());
		DaoManager daoMgr = DaoManager.INSTANCE;  	
		DaoUser  daoUser = (DaoUser) daoMgr.getDao(DB_Tabellen.USER);
	
		int userCount = daoUser.count();
		System.out.println("Anzahl der Datensätze " + DB_Tabellen.USER.toString() + "ist" + userCount );
		
		int U_id = 1;
		User user = daoUser.getRecord(U_id);
			if(user != null) {
				System.out.println("User mit der Id " +U_id+ "ist" + user.getFirstname() + ".");
				
			}else {
				System.out.println("Fehler aufgetreten");
			}
		}

	}
