
public class User {
	
	private int id;
	private String firstname;
	private String lastname;
	private String dateOfBirth;
	private String streetAndNumber;
	private int zipCode;
	private String residence;
	private String password;
	private String email;
	private int phoneNumber;
	private String rolle;
	
	//Konstruktor
	public User(int id, String firstname, String lastname, String dateOfBirth, String streetAndNumber, int zipCode,
			String residence, String password, String email, int phoneNumber, String rolle) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.dateOfBirth = dateOfBirth;
		this.streetAndNumber = streetAndNumber;
		this.zipCode = zipCode;
		this.residence = residence;
		this.password = password;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.rolle = rolle;
	}
	//Getter&Setter Methoden
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getStreetAndNumber() {
		return streetAndNumber;
	}

	public void setStreetAndNumber(String streetAndNumber) {
		this.streetAndNumber = streetAndNumber;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public String getResidence() {
		return residence;
	}

	public void setResidence(String residence) {
		this.residence = residence;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getRolle() {
		return rolle;
	}
	public void setRolle(String rolle) {
		this.rolle = rolle;
	}
}
