import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DaoUser extends DaoGeneric<User> {
	
	private final static String TABLENAME =DB_Tabellen.USER.toString(); 
	//Attribute 
	private final String A_ID = "u_id";
	private final String A_FIRSTNAME = "u_firstname";
	private final String A_LASTNAME  = "u_lastname";
	private final String A_DATEOFBIRTH = "u_dateOfBirth";
	private final String A_STREETANDNUMBER = "u_streetAndNumber";
	private final String A_ZIPCODE = "u_zipCode";
	private final String A_RESIDENCE = "u_residence";
	private final String A_PASSWORD = "u_password";
	private final String A_EMAIL ="u_email";
	private final String A_PHONENUMBER = "u_phoneNumber";
	private final String A_ROLLE = "u_rolle";
	
	public DaoUser(java.sql.Connection conn) {	//Konstruktor
		super(conn, TABLENAME);
	}
	
	@Override
	public int count() {
		String query = "SELECT COUNT(*) as count FROM" + TABLENAME;  //DB-abfrage
		PreparedStatement prepCounter;
		int recordCount = 0; 	//R�ckgabewert auf 0 gesetzt 								// wenn etwas schief gehen sollte, wird 0 zur�ckgegeben
		
		try {												//Datenbankabfrage
			prepCounter = this.conn.prepareStatement(query);	
			ResultSet rs = prepCounter.executeQuery(); //Abfrage wird durchgef�hrt
				if(rs.next()) {
					recordCount= rs.getInt("count");
				}
		}catch(Exception e) {
			e.printStackTrace();
			recordCount = -1; 	
		}
		return recordCount;
	}

	@Override
	public User getRecord(int id) {
		ResultSet rs = null;
		User selectedRecord = null;
		
		String query = "SELECT * FROM" + TABLENAME + "WHERE" + A_ID + "= ?";	//?= steht f�r einen beliebigen Wert
		try {
			PreparedStatement preStatement = conn.prepareStatement(query);
			preStatement.setInt(1, id);
			rs = preStatement.executeQuery();
			
				if(rs.next()) {
					selectedRecord = new User( 
							rs.getInt(A_ID),
							rs.getString(A_FIRSTNAME),
							rs.getString(A_LASTNAME),
							rs.getString(A_DATEOFBIRTH),
							rs.getString(A_STREETANDNUMBER),
							rs.getInt(A_ZIPCODE),
							rs.getString(A_RESIDENCE),
							rs.getString(A_PASSWORD),
							rs.getString(A_EMAIL),
							rs.getInt(A_PHONENUMBER	),
							rs.getString(A_ROLLE));
				}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return selectedRecord;
	}

}
