package LoginSystem;


import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class Login {

	private JFrame frame;
	private	JTextField txtUsername;
	private	JTextField txtPassword;
	private	JFrame frmLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setSize(1200,600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Login");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 50));
		lblNewLabel.setBounds(325,11,484,66);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lblUsername.setBounds(300,140,172,37);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Times New Roman", Font.BOLD, 30));
 		lblPassword.setBounds(300,260,170,45);
 		frame.getContentPane().add(lblPassword);
 		
 		txtUsername = new JTextField();
 		txtUsername.setFont(new Font("Times New Roman", Font.BOLD, 30));
 		txtUsername.setBounds(600,140,230,45);
 		frame.getContentPane().add(txtUsername);
 		txtUsername.setColumns(10);
 		
 		txtPassword = new JPasswordField();
 		txtPassword.setFont(new Font("Times New Roman" , Font.BOLD, 30));
 		txtPassword.setBounds(600,260,230,45);
 		frame.getContentPane().add(txtPassword);
 		
 		
 		JButton btnLogin = new JButton("Login");
 		btnLogin.setFont(new Font("Times New Roman", Font.BOLD, 30));
 		btnLogin.addActionListener(new ActionListener() {
 			public void actionPerformed(ActionEvent e) {
 				try {
 					Class.forName("com.mysql.jdbc.Driver");
 					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/userdaten", "root", "");
 					Statement stmt = con.createStatement();
 					String sql = "Select * From user WHERE Username='"+txtUsername.getText()+"'and Password='"+txtPassword.getText().toString()+"'";
 					ResultSet rs = stmt.executeQuery(sql);
 						if(rs.next())
 							JOptionPane.showMessageDialog(null, "erfolgreich eingeloggt");
 						else 
 							JOptionPane.showMessageDialog(null, "Fehler bei der Eingabe");
 						con.close();
 				}catch(Exception ex ) {
 					System.out.println(ex);
 				}
 		}
 	});
 		
 		btnLogin.setBounds(100,425,222,50);
 		frame.getContentPane().add(btnLogin);
 		
 		JButton btnReset = new JButton("Reset");
 		btnReset.setFont(new Font("Times New Roman", Font.BOLD,30));
 		btnReset.addActionListener(new ActionListener() {
 			public void actionPerformed(ActionEvent arg0) {
 				txtUsername.setText(null);
 				txtPassword.setText(null);
 			}
 		});
		btnReset.setBounds(465,425,222,50);
 		frame.getContentPane().add(btnReset);
 		
 		JButton btnExit = new JButton("Exit");
 		btnExit.setFont(new Font("Times New Roman", Font.BOLD, 30));
 		btnExit.addActionListener(new ActionListener() {
 			public void actionPerformed(ActionEvent e) {
 				frmLogin = new JFrame("Exit");
 					if(JOptionPane.showConfirmDialog(frmLogin, "Confirm if you want to exit", "Login Systems", 
 							JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
 						System.exit(0);
 					}
 			}
 		});
 		btnExit.setBounds(835,425,222,50);
 		frame.getContentPane().add(btnExit);
 		
 		JSeparator separator = new JSeparator();
 		separator.setBounds(28,242,484,5);
		
	}
}