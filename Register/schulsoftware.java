package com.company;

public class Main {

    public static void main(String[] args) {

//Schueler perspektive

        schueler bsp = new schueler();
        bsp.vorname = "Erich";
        bsp.nachname = "Matheus";
        bsp.email = "matheus.erich@gmax.com";
        bsp.passwort = "matti";
        bsp.passwortbestaetigen = "matti";
        bsp.alter = 20;
        bsp.geburtsdatum = 05.08;
        bsp.strasse = "javastraße";
        bsp.hausnummer = 2;
        bsp.stadt = "java";
        bsp.plz = 25445;
        bsp.telefonnummer = 01011101;
        bsp.id = 1;

        bsp.skript();
        bsp.krankmeldungen();
        bsp.bilder();
        bsp.profilbearbeiten();
        bsp.achievements();
        bsp.logout();

        //Lehrer perspektive

        lehrer exp = new lehrer();
        exp.vorname = "hans";
        exp.nachname = "dieter";
        exp.email = "dieter@gmail.com";
        exp.passwort = "dieter123";
        exp.passwortbestaetigen = "dieter123";
        exp.alter = 59;
        exp.geburtsdatum = 06.08;
        exp.strasse = "dieterstraße";
        exp.hausnummer = 55;
        exp.stadt = "dietrich";
        exp.plz = 23556;
        exp.telefonnummer = 001110;
        exp.id = 3;

        exp.skript();
        exp.krankmeldungen();
        exp.skripthochladen();
        exp.bilder();
        exp.profilbearbeiten();
        exp.anmerkungenverwalten();
        exp.hausaufgaben();
        exp.achievements();
        exp.logout();

        //Verwalter perspektive

        verwalter verw = new verwalter();
        verw.vorname = "Hans";
        verw.nachname = "Meier";
        verw.email = "hansmeiser@web.de";
        verw.passwort = "meier123";
        verw.passwortbestaetigen = "meier123";
        verw.alter = 63;
        verw.geburtsdatum = 05.07;
        verw.strasse = "Diedrichsstraße";
        verw.hausnummer = 58;
        verw.stadt = "Luebeck";
        verw.plz = 24554;
        verw.telefonnummer = 1110010;
        verw.id = 5;

        verw.stundenplanupdate();
        verw.skriptansehen();
        verw.krankmeldungen();
        verw.skripthochladenaendern();
        verw.gebaeudeplanhochladenloeschen();
        verw.stundenplanhochladenloeschen();
        verw.stundenplanerstellen();
        verw.galerieansehen();
        verw.datenschutzhinzufuegenupdaten();
        verw.bilderhinzufuegenloeschen();
        verw.profilbearbeiten();
        verw.achievementsansehen();
        verw.anmerkungverwalten();
        verw.hausaufgabenerstellenaendern();
        verw.impressumerstellenupdaten();
        verw.mensaplanerstellenupdaten();
        verw.nutzerregistrieren();
        verw.profilerstellt();
        verw.suche();
        verw.accountbearbeiten();
        verw.accountloeschen();

    }
}